CC = g++
CFLAGS  = -Wall -std=c++11
TARGET = rects

all: $(TARGET)
 
$(TARGET): main.o RectangleOperations.o	
	$(CC) $(CFLAGS) -o $(TARGET) main.o RectangleOperations.o 

main.o: main.cpp RectangleOperations.h
	$(CC) $(CFLAGS) -c main.cpp

json.o: json/json.h json/json-forwards.h
	$(CC) $(CFLAGS) -c jsoncpp.cpp
	
RectangleOperations.o: RectangleOperations.cpp RectangleOperations.h ShapeOperations.h ErrorHandler.h
	$(CC) $(CFLAGS) -c RectangleOperations.cpp

clean:
	$(RM) *.o $(TARGET)
