//
//  ShapeOperations.h
//  Intersections
//
//  Created by Tarik Rahman on 21/09/2016.
//
//

#ifndef ShapeOperations_h
#define ShapeOperations_h

using namespace std;

struct Shape
{
    Shape() : x(0), y(0){}
    int x;
    int y;
};

class ShapeOperations
{
public:
    virtual void findIntersections() = 0;
    virtual void printIntersections() = 0;
    virtual bool readInputFile(const string& filename) = 0;
    //virtual Shape calculateIntersection(const Shape& s1, const Shape& s2) = 0;
    virtual ~ShapeOperations(){};
};

#endif /* ShapeOperations_h */
