//
//  RectangleOperations.h
//  Intersections
//
//  Created by Tarik Rahman on 21/09/2016.
//
//

#ifndef RectangleOperations_h
#define RectangleOperations_h

#include <iostream>
#include <vector>
#include <map>
#include <set>

#include "ShapeOperations.h"
#include "ErrorHandler.h"

using namespace std;

const int MAX_RECTANGLES = 10;



struct Rectangle //: public Shape
{
    Rectangle() : x(0), y(0), width(0), height(0) {}
    Rectangle(int x_, int y_, int width_, int height_) :  x(x_), y(y_), width(width_), height(height_){}
    bool intersects(const Rectangle& r2)
    {
        // Using deMorgan's laws, r2 is left of this rectangle, then right of it, then above, then below
        return !(x > r2.x+r2.width || x+width < r2.x || y > r2.y+r2.height || y+height < r2.y);
    }
    
    Rectangle calculateIntersection(const Rectangle& r2)
    {
        int x1 = max(x, r2.x);
        int y1 = max(y, r2.y);
        int x2 = min(x+width,  r2.x + r2.width);
        int y2 = min(y+height, r2.y + r2.height);
        int width  = x2-x1;
        int height = y2-y1;
					   
        Rectangle intersectionRectangle(x1, y1, width, height);
        return intersectionRectangle;
    }
    
    string rectangleDetails()
    {
        return " at ("+to_string(x)+","+to_string(y)+"), w="+to_string(width)+", h="+to_string(height)+".";
    }
    
    int x;
    int y;
    int width;
    int height;
};


struct Intersection
{
    Rectangle rectangle;
    set<int> rectangleIndices;
};



class RectangleOperations : public ShapeOperations
{
public:
    RectangleOperations(const string& filename);
    virtual ~RectangleOperations();
    virtual void findIntersections();
    virtual void printIntersections();
    virtual bool readInputFile(const string& filename);
    bool intersects(const Rectangle& r1, const Rectangle& r2);
    //virtual Shape calculateIntersection(const Shape& s1, const Shape& s2);
    Rectangle calculateIntersection(const Rectangle& r1, const Rectangle& r2);
    virtual bool readJSonFile(const string& filename);
    void addNewIntersection(vector<Intersection*>& intersections,
                            const Rectangle& rect,
                            const set<int>& indices);
    void printRectangles();
    
private:
    vector<Rectangle*> m_rectangles;
    vector< vector<Intersection*> > m_listOfIntersectionLists;
    ErrorHandler errorHandler;
};

#endif /* RectangleOperations_h */
