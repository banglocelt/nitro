//
//  ErrorHandler.h
//  Intersections
//
//  Created by Tarik Rahman on 26/09/2016.
//
//

#ifndef ErrorHandler_h
#define ErrorHandler_h

const string ERROR_PREFIX = "ERROR : ";
const string INFO_PREFIX = "INFO : ";


class ErrorHandler
{
public:
    
    ErrorHandler(){};
    void readProblem(const string& extension)
    {
        cout<<ERROR_PREFIX<<"problem reading "<<extension<<" file"<<endl;
    }
    
    void extensionProblem(const string& extension, const string& filename)
    {
        cout<<ERROR_PREFIX<<"expecting "<<extension<<" file extension in "<<filename<<endl;
    }
    
    void noFieldFound(const string& fieldName)
    {
        cout<<ERROR_PREFIX<<"no Json field found called '"<<fieldName<<"'"<<endl;
    }
    
    void negativeValue(const int index)
    {
        cout<<ERROR_PREFIX<<"negative value for width or height. Ignoring rectangle "<<index<<endl;
    }
    
    void maxDataExceeded(const int max)
    {
        cout<<INFO_PREFIX<<"have read in "<<max<<" rectangles. Ignoring the rest."<<endl;
    }
    
    void generalError(const string& msg)
    {
        std::cerr<<ERROR_PREFIX<<msg<<endl;
    }
    
    
};

#endif /* ErrorHandler_h */
