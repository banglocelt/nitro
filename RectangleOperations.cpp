//
//  RectangleOperations.cpp
//  Intersections
//
//  Created by Tarik Rahman on 21/09/2016.
//
//

#include "RectangleOperations.h"
#include <algorithm>
#include <fstream>
#include "json.hpp"

using json = nlohmann::json;

namespace {
    const string JSON_EXTENSION = ".json";
    const string WIDTH_FIELD = "w";
    const string HEIGHT_FIELD = "h";
    const string RECTANGLES_NAME = "rects";
}

namespace utils
{
    bool endsWith(const string& str, const string& suffix)
    {
        int strSize    = str.size();
        int suffixSize = suffix.size();
    
        return (strSize >= suffixSize) && (str.compare(strSize - suffixSize, suffixSize, suffix) == 0);
    }
}

////////////////////////////////////////////////////////////////////////////////
RectangleOperations::RectangleOperations(const string& filename)
{
    bool result = readInputFile(filename);
    if(!result)
        exit(0);
}

////////////////////////////////////////////////////////////////////////////////
RectangleOperations::~RectangleOperations()
{
    for(unsigned  int i=0;i<m_rectangles.size();i++)
    {
        delete m_rectangles[i];
    }
    m_rectangles.clear();
    
    for(unsigned  int k=0;k<m_listOfIntersectionLists.size();++k)
    {
        vector<Intersection*> intersectionsList = m_listOfIntersectionLists[k];
        
        for(unsigned int i=0;i<intersectionsList.size();++i)
        {
            delete intersectionsList[i];
        }
    }
    m_listOfIntersectionLists.clear();
}

////////////////////////////////////////////////////////////////////////////////
bool RectangleOperations::readInputFile(const string& filename)
{
    bool result = true;
    if(utils::endsWith(filename, JSON_EXTENSION))
    {
        result = readJSonFile(filename);
        if(!result)
        {
//            cout<<"ERROR : problem reading "<<JSON_EXTENSION<<" file"<<endl;
            errorHandler.readProblem(JSON_EXTENSION);
        }
    }
    else
    {
   //     cout<<"ERROR : expecting "<<JSON_EXTENSION<<" file extension in "<<filename<<endl;
        errorHandler.extensionProblem(JSON_EXTENSION, filename);
        result = false;
    }
    return result;
}

////////////////////////////////////////////////////////////////////////////////
bool RectangleOperations::readJSonFile(const string& filename)
{
    ifstream inputFileStream(filename);
    
    try
    {
        json jsonFileObject;
        inputFileStream >> jsonFileObject;
    
        json jsonRects = jsonFileObject[RECTANGLES_NAME];
        if(jsonRects.size() == 0)
        {
            //cout<<"ERROR : No Json field found called '"<<RECTANGLES_NAME<<"'"<<endl;
            errorHandler.noFieldFound(RECTANGLES_NAME);
            return false;
        }
        
        int x      = 0;
        int y      = 0;
        int width  = 0;
        int height = 0;
        int count  = 0;
        
        for(json::iterator it = jsonRects.begin();it != jsonRects.end(); ++it)
        {
            if(m_rectangles.size() >= MAX_RECTANGLES)
            {
                errorHandler.maxDataExceeded(MAX_RECTANGLES);
                break;
            }
            
            json jsonRect = *it;
            x = jsonRect["x"];
            y = jsonRect["y"];
            width = jsonRect["w"];
            height = jsonRect["h"];
         //   cout<<"rect "<<count<<" x = "<<x<<"   y = "<<y<<"  width = "<<width<<"  height = "<<height<<endl;
        
            if( (width<=0) || (height<=0) )
            {
                //cout<<"ERROR : Negative value for width or height. Ignoring rectangle "<<count+1<<endl;
                errorHandler.negativeValue(count+1);
                continue;
            }
            
            Rectangle* rect = new Rectangle(x, y, width, height);
            m_rectangles.push_back(rect);
            count++;
        }
    }
    catch (const std::invalid_argument& invalidArg)
    {
        // bring up this with {{ instead of {
        std::cerr<<"invalid argument : "<<invalidArg.what()<<endl;
        errorHandler.generalError(invalidArg.what());
        return false;
    }
    catch (const std::domain_error& invalidArg)
    {
        std::cerr<<"domain error : "<<invalidArg.what()<<endl;
        // bring up this with typo for a field string e.g. ww instead of w
        errorHandler.generalError(invalidArg.what());
        return false;
    }
    
    return true;
}

/*
int RectangleOperations::readJsonField(const json& jsonObject, const string& fieldName)
{
    int val = jsonObject[fieldName];
    if(val<=0)
    {
        cout<<"ERROR : Negative value. See field '"<<fieldName<<"'. Ignoring this rectangle."<<endl;
        return -1;
    }
    return val;
}*/

////////////////////////////////////////////////////////////////////////////////
void printSet(const set<int>& iSet)
{
    set<int>::iterator iter;
    for(iter=iSet.begin(); iter!=iSet.end();++iter)
    {
        cout<<(*iter)<<" ";
    }
    cout<<endl;
}

////////////////////////////////////////////////////////////////////////////////
void RectangleOperations::findIntersections()
{
    m_listOfIntersectionLists.clear();
    
    vector<Intersection*> intersections;
    
    for(unsigned int i=0;i<m_rectangles.size();++i)
    {
        for(unsigned  int j=i+1;j<m_rectangles.size();++j)
        {
            Rectangle* r1 = m_rectangles[i];
            Rectangle* r2 = m_rectangles[j];
            //if(r1->intersects(*r2))
            if(intersects(*r1,*r2))
            {
 //               Rectangle rect = r1->calculateIntersection(*r2);
                Rectangle rect = calculateIntersection(*r1,*r2);
                set<int> indices;
                indices.insert(i);
                indices.insert(j);
                addNewIntersection(intersections, rect, indices);
            }
        }
    }
    m_listOfIntersectionLists.push_back(intersections);
    
    
    bool foundIntersection = true;
    // get the last list of intersections e.g. it could be the intersections between 2 rectangles
    // OR if it's the iteration after that, it would be the intersections between 3 rectangles.
    // Iterate over these intersections and see if they intersect with any rectangle
    while(foundIntersection)
    {
        foundIntersection = false;
        
        vector<Intersection*> lastIntersections = m_listOfIntersectionLists.back();
        vector<Intersection*> newIntersections;
        
        for(unsigned  int i=0;i<lastIntersections.size();++i)
        {
            Intersection* intersection = lastIntersections[i];
            Rectangle& r1              = intersection->rectangle;
            set<int> indices           = intersection->rectangleIndices;
            
            //cout<<"i = "<<i<<endl;
            for(unsigned  int j=0;j<m_rectangles.size();++j)
            {
                Rectangle* r2 = m_rectangles[j];
                
                // we don't want to check any rectangles that are already part of this intersection
                bool isInSet  = indices.count(j);
                if(isInSet)
                {
                    continue;
                }
                
                // now check if the set of (this rectangle index + current intersection indices)
                // have already been found in any new intersections' sets of indices
                // e.g we may be on an intersection of 3, 0, 4 but the set of 0, 3, 4 may already
                // be found as an intersection
                set<int> iSet = indices;
                iSet.insert(j);
                for(unsigned  int k=0;k<newIntersections.size();++k)
                {
                    set<int> newIndices = newIntersections[k]->rectangleIndices;
                    set<int> diff;
                    set_difference(iSet.begin(), iSet.end(),
                                   newIndices.begin(), newIndices.end(),
                                   inserter(diff, diff.end()));
                    
                    if(diff.size() == 0 )
                        isInSet = true;
                }
                if(isInSet)
                    continue;
                
                if(r1.intersects(*r2))
                {
                    Rectangle rect = r1.calculateIntersection(*r2);
                    //cout<<"Intersection between rectangles "<<j<<" ";
                    //printSet(indices);
                    //cout<<rect.rectangleDetails()<<endl;
            
                    foundIntersection = true;
                    set<int> iSet = indices;
                    iSet.insert(j);
                    addNewIntersection(newIntersections, rect, iSet);
                }
            }
        }

        if(newIntersections.size() > 0)
        {
            m_listOfIntersectionLists.push_back(newIntersections);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
bool RectangleOperations::intersects(const Rectangle& r1, const Rectangle& r2)
{
    // Using deMorgan's laws, r2 is left of this rectangle, then right of it, then above, then below
    return !(r1.x > r2.x+r2.width || r1.x+r1.width < r2.x || r1.y > r2.y+r2.height || r1.y+r1.height < r2.y);
}

////////////////////////////////////////////////////////////////////////////////
Rectangle RectangleOperations::calculateIntersection(const Rectangle& r1, const Rectangle& r2)
{
    int x1 = max(r1.x, r2.x);
    int y1 = max(r1.y, r2.y);
    int x2 = min(r1.x+r1.width,  r2.x + r2.width);
    int y2 = min(r1.y+r1.height, r2.y + r2.height);
    int width  = x2-x1;
    int height = y2-y1;
    
    Rectangle intersectionRectangle(x1, y1, width, height);
    return intersectionRectangle;
}

////////////////////////////////////////////////////////////////////////////////
void RectangleOperations::addNewIntersection(vector<Intersection*>& intersections,
                                             const Rectangle& rect,
                                             const set<int>& indices)
{
    Intersection* intersection = new Intersection();
    intersection->rectangle = rect;
    intersection->rectangleIndices = indices;
    intersections.push_back(intersection);
}

////////////////////////////////////////////////////////////////////////////////
void RectangleOperations::printRectangles()
{
    cout<<"Input:"<<endl;
    for(unsigned  int i=0;i<m_rectangles.size();++i)
    {
        Rectangle* rect = m_rectangles[i];
        cout<<"      "<<i+1<<": Rectangle"<<rect->rectangleDetails()<<endl;	
    }
    cout<<endl;
}

////////////////////////////////////////////////////////////////////////////////
void RectangleOperations::printIntersections()
{
    cout<<"Intersections"<<endl;
    for(unsigned  int k=0;k<m_listOfIntersectionLists.size();++k)
    {
        vector<Intersection*> intersectionsList = m_listOfIntersectionLists[k];
    
        for(unsigned  int i=0;i<intersectionsList.size();++i)
        {
            Intersection* intersection = intersectionsList[i];
            set<int> indices           = intersection->rectangleIndices;
            
            int size  = indices.size();
            int count = 0;
        
            cout<<"      Between rectangle ";
            
            set<int>::iterator setIter;

            for(setIter=indices.begin(); setIter!=indices.end(); ++setIter)
            {
                int rectangleIndex = (*setIter)+1;
                string postfix("");
                if(count == size-2)
                    postfix = " and ";
                else if(count != size-1)
                    postfix = ", ";
            
                cout<<rectangleIndex<<postfix;
                count++;
            }
            cout<<intersection->rectangle.rectangleDetails()<<endl;
        }
    }
}

