//
//  main.cpp
//  Intersections
//
//  Created by Tarik Rahman on 21/09/2016.
//
//

#include <iostream>
#include "RectangleOperations.h"

using namespace std;

int main(int argc, char *argv[])
{
    if(2 != argc)
    {
        cout<<"ERROR : program expects filename passed as 2nd argument"<<endl;
        return -1;
    }
    
   RectangleOperations rectangleOps(argv[1]);
    
    cout<<"printRectangles"<<endl;
    
   rectangleOps.printRectangles();
   rectangleOps.findIntersections();
   rectangleOps.printIntersections();
    
    return 0;
}
